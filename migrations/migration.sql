
CREATE TABLE books (
    id SERIAL PRIMARY KEY,
    name character varying(64)
);


CREATE UNIQUE INDEX books_pkey ON books(id int4_ops);

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name character varying(64)
);

CREATE UNIQUE INDEX users_pkey ON users(id int4_ops);


CREATE TABLE borrows (
    created_at timestamp with time zone,
    returned_at timestamp with time zone,
    score smallint,
    user_id integer REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
    book_id integer REFERENCES books(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT borrows_pkey PRIMARY KEY (user_id, book_id)
);


CREATE UNIQUE INDEX borrows_pkey ON borrows(user_id int4_ops,book_id int4_ops);
