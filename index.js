
import express from 'express'
import * as entities from './entities'

import UserController from './controllers/user.controller'
import BookController from './controllers/book.controller'

const main = async () => {

  const conn = await entities.connectAndMigrate()

  const app = express()
  
  
  

  app.use('/users', UserController)
  app.use('/books', BookController)



  app.listen(process.env.PORT || 3000, () => console.log('Started'))


}

main()