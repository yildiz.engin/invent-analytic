import _ from 'lodash'
import * as express from 'express'
import * as bodyParser from 'body-parser'
import { Book, sequelize } from '../entities'

const router = express.Router()

router.post('', bodyParser.json(), async (req, res) => {
  const params = req.body || {}

  try {
    await Book.validate(params)
    const book = await Book.create(params)
    res.sendStatus(201)
  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

router.get('', async (req, res) => {

  try {
    const books = await Book.findAll()
    res.json(books)
  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

router.get('/:bookId([0-9]+)', async (req, res) => {
  const { bookId } = req.params

  try {
    const book = await Book.findByPk(bookId, {
      attributes: [
        'id',
        'name',
        [sequelize.literal('(SELECT ROUND(COALESCE(AVG(score), -1), 2) FROM borrows WHERE book_id = books.id)'), 'score']
      ]
    })
    if (book) {
      res.json(book)
    } else {
      throw Error('Book not found!')
    }
  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

export default router
