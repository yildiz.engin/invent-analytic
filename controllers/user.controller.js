import _ from 'lodash'
import * as express from 'express'
import * as bodyParser from 'body-parser'
import { Op } from 'sequelize'
import { User, Book, Borrow } from '../entities'


const router = express.Router()

router.post('', bodyParser.json(), async (req, res) => {
  const params = req.body || {}

  try {
    await User.validate(params)
    await User.create(params)
    res.sendStatus(201)
  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

router.get('', async (req, res) => {

  try {
    const users = await User.findAll()
    res.json(users)
  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

router.get('/:userId([0-9]+)', async (req, res) => {
  const { userId } = req.params

  try {
    const user = await User.findByPk(userId)
    if (user) {
      const books = await user.getBooks()

      res.json(_.assign(user.get({ plain: true }), {
        books: {
          past: books.filter(b => b.borrows.returned_at).map(b => ({ name: b.name, userScore: b.borrows.score })),
          present: books.filter(b => !b.borrows.returned_at).map(b => ({ name: b.name })),
        }
      }))
    } else {
      throw Error('User not found!')
    }
  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

router.post('/:userId([0-9]+)/borrow/:bookId([0-9]+)', async (req, res) => {
  const { userId, bookId } = req.params

  try {

    const [user, book, available] = await Promise.all([
      User.findByPk(userId),
      Book.findByPk(bookId),
      Borrow.count({ where: { book_id: bookId, [Op.or]: [{ returned_at: null }, { user_id: userId }] } }).then(count => count === 0)
    ])

    if (!book) {
      throw Error('Book not found!')
    } else if (!user) {
      throw Error('User not found!')
    } else if (!available) {
      throw Error('The book is already borrowed!')
    }

    await user.addBook(book)

    res.sendStatus(204)

  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

router.post('/:userId([0-9]+)/return/:bookId([0-9]+)', bodyParser.json(), async (req, res) => {

  const { userId, bookId } = req.params

  try {

    await Borrow.validate(req.body)

    const borrow = await Borrow.findOne({ where: { book_id: bookId, user_id: userId, returned_at: null } })

    if (!borrow) {
      throw Error('Operation failed!')
    }

    await borrow.update(_.assign(req.body, { returned_at: new Date() }))

    res.sendStatus(204)

  } catch (err) {
    res.status(500).json({ message: err.message })
  }

})

export default router