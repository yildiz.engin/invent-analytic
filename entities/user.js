import Joi from '@hapi/joi'
import { DataTypes, Model } from 'sequelize'


const schema = Joi.object({
  name: Joi.string().min(3).required()
});


class User extends Model {

  /**
   * 
   * @param {User} user 
   */
  static async validate(user) {

    
    return schema.validateAsync(user)
  }
}


/**
 * @param {Sequelize} sequelize
 * @returns {User}
 * 
 */
export default (sequelize) => {

  return User.init({
    name: {
      type: DataTypes.STRING(64),
      field: 'name'
    }
  }, {
    sequelize,
      modelName: 'users',
      timestamps: false
  })
}
