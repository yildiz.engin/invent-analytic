import Joi from '@hapi/joi'
import { DataTypes, Model } from 'sequelize'

const schema = Joi.object({
  score: Joi.number()
    .integer()
    .min(0)
    .max(10)
    .required()
})


class Borrow extends Model {

  /**
   * 
   * @param {Borrow} borrow
   */
  static async validate(borrow) {
    return schema.validateAsync(borrow)
  }
}


/**
 * @param {Sequelize} sequelize
 * @returns {Borrow}
 * 
 */
export default (sequelize) => {

  return Borrow.init({

    created_at: {
      type: DataTypes.DATE,
      field: 'created_at',
      defaultValue: DataTypes.NOW
    },
    returned_at: {
      type: DataTypes.DATE,
      field: 'returned_at',
    },
    score: {
      type: DataTypes.SMALLINT,
      field: 'score',
    },
  }, {
    sequelize,
    modelName: 'borrows',
    timestamps: false
  })
}
