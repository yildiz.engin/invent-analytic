

import { Sequelize } from 'sequelize'

import createUserModel from './user'
import createBookModel from './book'
import createBorrowModel from './borrow'


export const sequelize = new Sequelize(process.env.DB || 'postgres://postgres:petlas@localhost/library')


export const User = createUserModel(sequelize)
export const Book = createBookModel(sequelize)
export const Borrow = createBorrowModel(sequelize)


export const connectAndMigrate = async (force = false) => {

  User.Book = User.belongsToMany(Book, { through: Borrow, foreignKey: 'user_id' })
  Book.User = Book.belongsToMany(User, { through: Borrow, foreignKey: 'book_id', as: 'users'  })

  return sequelize.sync({ force: force })
}