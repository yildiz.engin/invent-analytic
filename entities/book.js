
import Joi from '@hapi/joi'
import { DataTypes, Model } from 'sequelize'

const schema = Joi.object({
  name: Joi.string().min(3).required()
});

class Book extends Model {

  /**
   * 
   * @param {Book} book 
   */
  static async validate(book) {
    return schema.validateAsync(book)
  }
}


/**
 * @param {Sequelize} sequelize
 * @returns {Book}
 * 
 */
export default (sequelize) => {

  return Book.init({
    name: {
      type: DataTypes.STRING(64),
      field: 'name'
    }
  }, {
    sequelize,
      modelName: 'books',
      timestamps: false
  })
}
