import { connectAndMigrate, User, Book, Borrow } from '../entities'

describe('entities', () => {

  let conn;

  beforeAll(async () => {
    conn = await connectAndMigrate(true)
  });

  afterAll(async () => {
    conn.connectionManager.close()
  })


  it('create new user', async () => {

    const user = await User.create({ name: 'Engin Yıldız' });
  
    const createdUser = await User.findOne({ where: { name: 'Engin Yıldız' } });
    expect(user.id).toEqual(createdUser.id);
  });

  it('create a new book', async () => {

    const book = await Book.create({ name: 'Cosmos by Carl Sagan' });

    const createdBook = await Book.findOne({ where: { name: 'Cosmos by Carl Sagan' } });
    expect(book.id).toEqual(createdBook.id);
  });

  it('user borrowed a new book', async () => {

    const user = await User.findOne({ where: { name: 'Engin Yıldız' } })
    const book = await Book.findOne({ where: { name: 'Cosmos by Carl Sagan' } });

    await user.addBook(book)

    expect((await user.getBooks()).length).toEqual(1);
  });


  it('a book returned', async () => {

    const borrow = await Borrow.findOne({ where: { user_id: 1, book_id: 1 } })

    await borrow.update({ returned_at: new Date(), score: 9 })

    expect(borrow.score).toEqual(9);
  });
});